package br.ufu.si.sd.entidades;

/**
 * Created by vitor on 17/09/17
 */
public class Config {

    private Integer port;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
