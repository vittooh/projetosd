package br.ufu.si.sd.entidades;

/**
 * Created by vitor on 17/09/17
 */
public class ConfigClient extends Config {

    private String hostname;


    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
