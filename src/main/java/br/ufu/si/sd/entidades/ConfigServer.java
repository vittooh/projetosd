package br.ufu.si.sd.entidades;

/**
 * Created by vitor on 17/09/17
 */
public class ConfigServer extends Config {


    private Integer clientTimeout;

    public Integer getClientTimeout() {
        return clientTimeout;
    }

    public void setClientTimeout(Integer clientTimeout) {
        this.clientTimeout = clientTimeout;
    }
}
