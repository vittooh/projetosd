package br.ufu.si.sd.entidades;

import java.util.List;

/**
 * Created by vitor on 24/09/17
 */
public class Instrucoes {

    private List<String> insertInstrucoes;
    private List<String> insertUpdate;


    public List<String> getInsertInstrucoes() {
        return insertInstrucoes;
    }

    public void setInsertInstrucoes(List<String> insertInstrucoes) {
        this.insertInstrucoes = insertInstrucoes;
    }

    public List<String> getInsertUpdate() {
        return insertUpdate;
    }

    public void setInsertUpdate(List<String> insertUpdate) {
        this.insertUpdate = insertUpdate;
    }

}
