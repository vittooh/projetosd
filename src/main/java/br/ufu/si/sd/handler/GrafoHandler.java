package br.ufu.si.sd.handler;

import br.ufu.si.sd.services.*;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;

/**
 * Created by vitor on 17/09/17
 */
public class GrafoHandler implements Grafo.Iface {

    private static Logger logger = Logger.getLogger(GrafoHandler.class.getName());

    private static final ConcurrentHashMap<String, Aresta> arestasBD = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Integer, Vertice> verticesDB = new ConcurrentHashMap<>();

    @Override
    public boolean criaVertice(Vertice v) throws EntradaDuplicada, TException {
        logger.info("Tentando criar Vertice : " + v.toString());
        if (verticesDB.containsKey(v.getId())) throw new EntradaDuplicada();
        //double check
        return verticesDB.putIfAbsent(v.getId(), v) != null;
    }


    @Override
    public Vertice getVertice(int id) throws IDdNaoEncontrado, TException {
        logger.info("tentando pegar o vertice com  id " + id);
        Vertice vertice = verticesDB.get(id);
        if (vertice == null) throw new IDdNaoEncontrado();
        return vertice;
    }


    @Override
    public boolean removeVertice(int id) throws IDdNaoEncontrado, TException {
        logger.info("tentando remover vertice com id " + id);
        if (!verticesDB.containsKey(id)) {
            logger.severe("id não encontrado id  =   " + id);
            throw new IDdNaoEncontrado();
        }

        Vertice verticeRemovido = verticesDB.get(id);
        synchronized (verticeRemovido) {
            logger.info("Entrou no try do remove vertice");

            List<Map.Entry<String, Aresta>> arestasRemocao = arestasBD.entrySet().stream()
                    .filter(aresta ->
                            aresta.getValue().getV1() == verticeRemovido.getId()
                                    || aresta.getValue().getV2() == verticeRemovido.getId()
                    ).collect(toList());

            return (arestasRemocao != null && !arestasRemocao.isEmpty()
                    && arestasBD.entrySet().removeAll(arestasRemocao));
        }
    }


    //usuário pode alterar o id logo eu so pego o que pode ser modificado
    @Override
    public Vertice updateVertice(Vertice v) throws IDdNaoEncontrado, TException {
        logger.info("tentando fazer update do vertice  " + v);

        Vertice vertice = verticesDB.get(v.getId());
        if (vertice == null) throw new IDdNaoEncontrado();
        synchronized (vertice) {
            vertice.setCor(v.getCor());
            vertice.setDescricao(v.getDescricao());
            vertice.setPeso(v.getPeso());
            return verticesDB.put(v.getId(), vertice);
        }
    }

    @Override
    public boolean criaAresta(Aresta a) throws VerticesNaoEncontrados, TException {
        logger.info("Tentando criar aresta :  " + a);

        Vertice verticeA = verticesDB.get(a.getV1());
        Vertice verticeB = verticesDB.get(a.getV2());
        if (verticeA == null || verticeB == null) throw new VerticesNaoEncontrados();
        String id = String.valueOf(verticeA.getId()) + String.valueOf(verticeB.getId());
        if (arestasBD.containsKey(id)) {
            logger.severe("Chave duplicada " + id + "Não modificando valor ");
            throw new EntradaDuplicada();
        }
        synchronized (verticeA) {
            synchronized (verticeB) {
                return arestasBD.putIfAbsent(id, a) != null;
            }
        }
    }


    //dead lock
    // um entra e da lock 12 13
    // outro entra e da lock 13 14
    @Override
    public boolean removeAresta(int id) throws IDdNaoEncontrado, TException {
        logger.info("Tentando remover aresta " + id);
        String key = String.valueOf(id);
        if (!arestasBD.containsKey(key)) throw new IDdNaoEncontrado();
        Aresta toRemove = arestasBD.get(key);
        synchronized (toRemove) {
            Vertice a = getVertice(toRemove.getV1());
            Vertice b = getVertice(toRemove.getV2());
            synchronized (a) {
                synchronized (b) {
                    return arestasBD.remove(String.valueOf(id)) != null;
                }
            }
        }
    }


    @Override
    public Aresta getAresta(int idVerticeA, int idVerticeB) throws IDdNaoEncontrado, TException {
        logger.info("Tentando pegar aresta com ids de vertice " + idVerticeA + "| " + idVerticeB);
        String id = String.valueOf(idVerticeA) + String.valueOf(idVerticeB);
        if (!arestasBD.containsKey(id)) throw new IDdNaoEncontrado();
        return arestasBD.get(id);
    }

    @Override
    public Aresta updateAresta(Aresta a) throws IDdNaoEncontrado, TException {
        // naõ vai subir ninguem teoricamente
        logger.info("Tentando fazer update em " + a);

        synchronized (a) {
            String id = String.valueOf(a.getV1()) + String.valueOf(a.getV2());
            Aresta arestaUp = arestasBD.get(id);
            if (arestaUp == null)
                throw new IDdNaoEncontrado();
            arestaUp.setDirecionada(a.isDirecionada());
            arestaUp.setPeso(a.getPeso());
            arestaUp.setDescricao(a.getDescricao());
            return arestasBD.put(id, arestaUp);
        }
    }


// metodos abaixo são thread safe ? ? ? / /

    @Override
    public List<Aresta> listaArestas() throws TException {

        synchronized (this) {
            logger.info("Listando arestas");
            return arestasBD.entrySet().stream().map(Map.Entry::getValue).collect(toList());
        }
    }

    @Override
    public List<Aresta> listaArestasVertices(int idVertice) throws TException {
        logger.info("Listando Arestas que pertecem ao vertice com id " + idVertice);

        List<Aresta> arestasAssociadas = new ArrayList<>();

        arestasAssociadas.addAll(arestasBD.entrySet().stream()
                .map(HashMap.Entry::getValue)
                .filter(aresta -> aresta.getV1() == idVertice || aresta.getV1() == idVertice)
                .collect(toList())
        );
        return arestasAssociadas;
    }

    @Override
    public List<Vertice> listaVertices() throws TException {
        return verticesDB.entrySet().stream().map(Map.Entry::getValue).collect(toList());
    }

    //sucesso
    @Override
    public List<Vertice> listaVerticesVizinhos(int idVertice) throws TException {

        logger.info("tentando pegar vertices vizinhos do " + idVertice);
        List<Aresta> arestas = listaArestasVertices(idVertice);

        List<Integer> ids = new ArrayList<>();

        ids.addAll(arestas.stream().filter(aresta ->
                aresta.getV2() != idVertice).map(Aresta::getV2).collect(toList()));

        ids.addAll(arestas.stream().filter(aresta ->
                aresta.getV1() != idVertice).map(Aresta::getV1).collect(toList()));

        List<Vertice> vertices = new ArrayList<>();

        ids.forEach(it -> {
            try {
                vertices.add(getVertice(it));
            } catch (TException e) {
                e.printStackTrace();
            }
        });
        return vertices;
    }
}
