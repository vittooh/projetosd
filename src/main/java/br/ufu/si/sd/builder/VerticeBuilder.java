package br.ufu.si.sd.builder;

import br.ufu.si.sd.entidades.Instrucoes;
import br.ufu.si.sd.services.Vertice;

import java.util.Scanner;

/**
 * Created by vitor on 24/09/17
 */
public class VerticeBuilder implements Crud<Vertice> {

    private Instrucoes instrucoesVertice;

    public VerticeBuilder(Instrucoes instrucoesVertice) {
        this.instrucoesVertice = instrucoesVertice;
    }

    @Override
    public Vertice buildInsert() {
        Scanner sc = new Scanner(System.in);
        Vertice newVertice = new Vertice();
        System.out.println("Obedeça a sequência de instruções");
        for (String instruct : instrucoesVertice.getInsertInstrucoes()) {
            System.out.println(instruct);
        }

        newVertice.setId(Integer.parseInt(sc.nextLine()));
        newVertice.setPeso(Double.parseDouble(sc.nextLine()));
        newVertice.setDescricao(sc.nextLine());
        newVertice.setCor(Integer.parseInt(sc.nextLine()));
        return newVertice;
    }

    @Override
    public Vertice buildUpdate(Vertice value) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Obedeça a sequência de instruções");
        for (String instruct : instrucoesVertice.getInsertUpdate()) {
            System.out.println(instruct);
        }
        value.setDescricao(sc.nextLine());
        value.setPeso(Double.parseDouble(sc.nextLine()));
        value.setCor(Integer.parseInt(sc.nextLine()));
        return value;
    }

}
