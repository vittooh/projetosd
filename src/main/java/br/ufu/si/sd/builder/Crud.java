package br.ufu.si.sd.builder;

/**
 * Created by vitor on 24/09/17
 */
public interface Crud<T> {

    T buildInsert();

    T buildUpdate(T value);
}
