package br.ufu.si.sd.builder;

import br.ufu.si.sd.entidades.Instrucoes;
import br.ufu.si.sd.services.Aresta;

import java.util.Scanner;

/**
 * Created by vitor on 24/09/17
 */
public class ArestaBuilder implements Crud<Aresta> {

    private Instrucoes instrucoesAresta;

    public ArestaBuilder(Instrucoes instrucoesAresta) {
        this.instrucoesAresta = instrucoesAresta;
    }

    @Override
    public Aresta buildInsert() {
        Scanner valuesRead = new Scanner(System.in);
        Aresta a = new Aresta();
        System.out.println("Siga a ordem de inserção!!");
        for (String instrucao : instrucoesAresta.getInsertInstrucoes()) {
            System.out.println(instrucao);
        }
        a.setV1(Integer.parseInt(valuesRead.nextLine()));
        a.setV2(Integer.parseInt(valuesRead.nextLine()));
        a.setDescricao(valuesRead.nextLine());
        a.setPeso(Double.parseDouble(valuesRead.nextLine()));
        a.setDirecionada(Integer.parseInt(valuesRead.nextLine()) == 1);
        return a;
    }

    @Override
    public Aresta buildUpdate(Aresta a) {
        Scanner valuesRead = new Scanner(System.in);
        System.out.println("Siga a ordem de inserção!!");
        for (String instrucao : instrucoesAresta.getInsertUpdate()) {
            System.out.println(instrucao);
        }
        a.setDescricao(valuesRead.nextLine());
        a.setPeso(Double.parseDouble(valuesRead.nextLine()));
        a.setDirecionada(Integer.parseInt(valuesRead.nextLine()) == 1);
        return a;
    }
}
