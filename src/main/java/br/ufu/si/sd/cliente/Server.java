package br.ufu.si.sd.cliente;


import br.ufu.si.sd.entidades.ConfigServer;
import br.ufu.si.sd.handler.GrafoHandler;
import br.ufu.si.sd.services.Grafo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Logger;

/**
 * Created by vitor on 24/09/17
 */
public class Server {
    public static Grafo.Processor processor;
    public static GrafoHandler grafoHandler;

    public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
        Logger logger = Logger.getLogger(Server.class.getName());
        grafoHandler = new GrafoHandler();
        processor = new Grafo.Processor(grafoHandler);
        ConfigServer configServer = null;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ClassLoader loader = ClassLoader.getSystemClassLoader();

        //agora o argumento é a linha do arquivo json
        configServer = gson.fromJson(args[0], ConfigServer.class);


        assert configServer != null;
        try {
            TServerTransport transport = new TServerSocket(configServer.getPort());
            TThreadPoolServer.Args a = new TThreadPoolServer.Args(transport).processor(processor);
            a.maxWorkerThreads(5);

            TServer server = new TThreadPoolServer(a);
            logger.info("Starting Server");
            server.serve();
        } catch (TTransportException e) {
            e.printStackTrace();
        }
    }
}
