package br.ufu.si.sd;


import br.ufu.si.sd.builder.ArestaBuilder;
import br.ufu.si.sd.builder.VerticeBuilder;
import br.ufu.si.sd.entidades.ConfigClient;
import br.ufu.si.sd.entidades.Instrucoes;
import br.ufu.si.sd.services.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by vitor on 17/09/17
 */
public class MainClient {
    public static void main(String[] args) {

        String instrucoes = "Bem vindo !!\n" +
                "Digite 1 para cadastrar uma aresta\n" +
                "Digite 2 para atualizar uma aresta\n" +
                "Digite 3 para remover uma aresta\n" +
                "Digite 4 para cadastrar um Vertice\n" +
                "Digite 5 para atualizar um Vertice\n" +
                "Digite 6 para remover um Vertice\n" +
                "Digite 7 Listar todos os Vertices \n" +
                "Digite 8 Listar todos as Arestas\n" +
                "Digite 9 Listar Arestas de um vertice\n" +
                "Digite 10 Listar Vertices Vizinhos de um  vertice\n";


        Logger logger = Logger.getLogger(MainClient.class.getName());

        Instrucoes instrucoesAresta = new Instrucoes();
        Instrucoes instrucoesVertice = new Instrucoes();

        ClassLoader loader = ClassLoader.getSystemClassLoader();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        ConfigClient c = null;
        try {
            //todo ver com lazaro server caiu cliente conectado tem como reconectar o cliente ??
            c = gson.fromJson(new FileReader(args[0]), ConfigClient.class);
            instrucoesAresta = gson.fromJson(new FileReader(args[1]), Instrucoes.class);
            instrucoesVertice = gson.fromJson(new FileReader(args[2]), Instrucoes.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArestaBuilder arestaBuilder = new ArestaBuilder(instrucoesAresta);
        VerticeBuilder verticeBuilder = new VerticeBuilder(instrucoesVertice);

        assert c != null;
        TTransport transport = null;
        try {
            transport = new TSocket(c.getHostname(), c.getPort());
            transport.open();
            TProtocol protocol = new TBinaryProtocol(transport);
            Grafo.Client client = new Grafo.Client(protocol);
            Scanner read = new Scanner(System.in);
            Scanner valuesRead = new Scanner(System.in);
            System.out.println(instrucoes);
            while (true) {
                //todo check this cast to int scanner

                String va = read.nextLine();
                if (va.equals("")) continue;
                Integer i = null;
                try {
                    i = Integer.valueOf(va);
                } catch (InputMismatchException ime) {
                    logger.severe("Opção inválida, são aceitos apenas números");
                    i = 11;
                }
                switch (i) {
                    case 1: {
                        Aresta a = null;
                        try {
                            a = arestaBuilder.buildInsert();
                            client.criaAresta(a);
                        } catch (TException e) {
                            if (e instanceof VerticesNaoEncontrados) {
                                logger.log(Level.SEVERE, String.format("Atenção !! algum dos vertices |%s| - |%s| não existe!! Não vou inserir"
                                        , a.getV1(), a.getV2()));
                            } else {
                                if (e instanceof EntradaDuplicada) {
                                    logger.severe("Tentando inserir aresta com chave duplicada " + a);
                                } else {
                                    logger.severe("Alguma merda aconteceu tentando inserir a aresta " + a);
                                }
                            }
                        }
                        break;
                    }
                    case 2: {
                        Integer verticea = null;
                        Integer verticeb = null;
                        System.out.println("Forneça o id dos Vertices que formam a aresta");
                        try {
                            verticea = valuesRead.nextInt();
                            verticeb = valuesRead.nextInt();
                            Aresta a = client.getAresta(verticea, verticeb);
                            client.updateAresta(arestaBuilder.buildUpdate(a));
                            System.out.println("Atualizado com sucesso " + a.toString());
                        } catch (TException e) {
                            if (e instanceof VerticesNaoEncontrados) {
                                logger.severe(String.format("Ids %s %s para vertices não encontrados", verticea, verticeb));
                            }
                            logger.severe("Algo");
                        }
                        break;
                    }
                    case 3: {
                        Integer id = null;
                        System.out.println("Forneça o id dos Vertices que formam a aresta");
                        try {
                            id = Integer.parseInt(valuesRead.nextLine());
                            client.removeAresta(id);
                            System.out.println("Removido com sucesso " + id);
                        } catch (TException e) {
                            logger.severe("Algo de estranho ocorreu ao executar a operação de remoção da aresta com id :  " + id);
                        }
                        break;
                    }
                    case 4: {
                        Vertice v = null;
                        try {
                            v = verticeBuilder.buildInsert();
                            client.criaVertice(v);
                        } catch (TException e) {
                            if (e instanceof EntradaDuplicada) {
                                logger.severe(String.format("id duplicado %s NÃO VOU INSERIR LALALA", v));
                            } else
                                logger.severe("Algo de errado ocorreu ao inserir o vertice" + v);
                        }
                        break;
                    }
                    case 5: {
                        System.out.println("Forneça o id do vertice");
                        Integer id = null;
                        try {
                            id = Integer.parseInt(valuesRead.nextLine());
                            Vertice vertice = client.getVertice(id);
                            client.updateVertice(verticeBuilder.buildUpdate(vertice));
                            System.out.println(String.format("Vertice %s atualizado com sucesso", vertice.getId()));
                        } catch (TException e) {
                            if (e instanceof IDdNaoEncontrado)
                                logger.severe(String.format("Não encontrei esse id %s", id));
                        }
                        break;
                    }
                    case 6: {
                        Integer key = null;
                        System.out.println("Forneça o id do vertice a ser excluido");
                        try {
                            key = Integer.valueOf(valuesRead.nextLine());
                            logger.info(String.format("Resultado da deleção > %s", client.removeVertice(key)));
                        } catch (TException e) {
                            logger.severe(String.format("ID nao existe %s", key));
                        }
                        break;
                    }
                    case 7:
                        try {
                            List<Vertice> vertices = client.listaVertices();
                            if (vertices.isEmpty()) System.out.println("Nao há vertices no BD");
                            else {
                                vertices.forEach(System.out::println);
                            }
                        } catch (TException e) {
                            e.printStackTrace();
                            protocol.reset();
                        }
                        break;
                    case 8:
                        try {
                            List<Aresta> arestas = client.listaArestas();
                            if (arestas.isEmpty())
                                System.out.println("Não foram encontradas arestas no BD ");
                            else
                                arestas.forEach(System.out::println);
                        } catch (TException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 9:
                        System.out.println("Forneça o id do vertice ");
                        try {
                            int id = Integer.parseInt(valuesRead.nextLine());
                            List<Aresta> arestas = client.listaArestasVertices(id);
                            if (arestas.isEmpty())
                                System.out.println("Não foram encontradas arestas para o vertice " + id);
                            else
                                arestas.forEach(System.out::println);
                        } catch (TException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 10:
                        System.out.println("Forneça  o id do vertice para pegar os vizinhos");
                        try {
                            int id = Integer.parseInt(valuesRead.nextLine());
                            List<Vertice> vertices = client.listaVerticesVizinhos(id);
                            if (vertices.isEmpty())
                                System.out.println(String.format("Vertice %s  não possui vizinhos", id));
                            else
                                vertices.forEach(System.out::println);
                        } catch (TException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
                System.out.println(instrucoes);
            }
        } catch (Throwable e) {
            logger.severe("Cliente morrendo Exceção não tratada");
            assert transport != null;
            transport.close();
        }
    }
}
