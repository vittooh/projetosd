namespace java code

struct Vertice{
	1: required i32 id;
	2: i32 cor;
	3: double peso;
	4: string descricao
}
struct Aresta{
	1: required i32 v1;
	2: required i32 v2;
	3: double peso;
	4: bool direcionada;
	5: string descricao;
}
exception InvalidGraphException {
	1: string message
}
struct ArestaKey{
	1: i32 v1;
	2: i32 v2;
	3: bool direcionada;
}

//
exception AcessoInvalidoBD{
}

// tanto para vertice quanto para aresta
exception EntradaDuplicada{
}

// tanto para aresta quanto para vertice
exception IDdNaoEncontrado{
}


exception VerticesNaoEncontrados{
}

service Grafo{
   bool criaVertice(1:Vertice v) throws (1:EntradaDuplicada e);
   Vertice getVertice(1: i32 id) throws(1 : IDdNaoEncontrado e);
   bool removeVertice(1: i32 id) throws(1 : IDdNaoEncontrado e);
   Vertice updateVertice(1:Vertice v) throws(1 : IDdNaoEncontrado e);
   bool criaAresta(1:Aresta a) throws (1:VerticesNaoEncontrados e);
   bool removeAresta(1: i32 id) throws(1 : IDdNaoEncontrado e);
   Aresta getAresta(1: i32 idVerticeA, 2:i32 idVerticeB) throws(1 : IDdNaoEncontrado e);
   Aresta updateAresta(1:Aresta a) throws(1 : IDdNaoEncontrado e);
   list<Aresta> listaArestas();
   list<Aresta> listaArestasVertices(1:i32 idVertice);
   list<Vertice> listaVertices();
   list<Vertice> listaVerticesVizinhos(1:i32 idVertice);
}


