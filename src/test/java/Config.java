import br.ufu.si.sd.entidades.ConfigClient;
import br.ufu.si.sd.entidades.ConfigServer;
import br.ufu.si.sd.entidades.Instrucoes;
import com.google.gson.Gson;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vitor on 17/09/17
 */
public class Config<T> {


    public void criaConfigCliente() {
        ConfigClient configClient = new ConfigClient();
        configClient.setHostname("localhost");
        configClient.setPort(9000);

        try {
            FileWriter writer = new FileWriter("/tmp/configCliente.json");
            Gson gson = new Gson();

            writer.write(gson.toJson(configClient));
            writer.close();
        } catch (Exception ignored) {

        }

    }

    public void criaConfigServidor() {
        ConfigServer configServer = new ConfigServer();
        configServer.setClientTimeout(30);
        configServer.setPort(8080);

        FileWriter writer = null;
        try {
            writer = new FileWriter("/tmp/configServer.json");
            Gson gson = new Gson();
            writer.write(gson.toJson(configServer));

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void run() {
        List<String> instrucoes = new ArrayList<>();

        instrucoes.add("Digite o Vertice A");
        instrucoes.add("Digite o Vertice B");
        instrucoes.add("Digite a descrição da Aresta");
        instrucoes.add("Digite  peso da Aresta");
        instrucoes.add("Essa aresta é bi Direcional :  |Sim = 1 | Não = 0");

        Instrucoes i = new Instrucoes();
        i.setInsertInstrucoes(instrucoes);
        i.setInsertUpdate(Arrays.asList("Digite a descrição da Aresta", "Digite  peso da Aresta",
                "Essa aresta é bi Direcional :  |Sim = 1 | Não = 0"));


        try {
            FileWriter writer = new FileWriter("/tmp/arestaInstrucoes.json");
            Gson gson = new Gson();

            writer.write(gson.toJson(i));
            writer.close();
        } catch (Exception ignored) {

        }
    }

    @Test
    public void geraInstrucoesVertice() {

        List<String> instrucoes = new ArrayList<>();

        instrucoes.add("Digite o ID do vertice");
        instrucoes.add("Digite o Peso do vertice");
        instrucoes.add("Digite a descrição do vertice");
        instrucoes.add("Digite a cor do vertice");


        Instrucoes i = new Instrucoes();
        i.setInsertInstrucoes(instrucoes);
        i.setInsertUpdate(Arrays.asList("Digite a descrição do Vertice", "Digite  peso do  Vertice",
                "Digite  a cor Do vertice"));

        try {
            FileWriter writer = new FileWriter("/tmp/verticeInstrucoes.json");
            Gson gson = new Gson();

            writer.write(gson.toJson(i));
            writer.close();
        } catch (Exception ignored) {

        }
    }


}
